package math;

public class Calculator {
	int number_1;
	int number_2;
	String operation;
		public int getNumber_1() {
			return number_1;
		}
		
		public void setNumber_1(int number_1) {
			this.number_1 = number_1;
		}
		
		public int getNumber_2() {
			return number_2;
		}
		
		public void setNumber_2(int number_2) {
			this.number_2 = number_2;
		}
		
		public String getOperation() {
			return operation;
		}
		
		public void setOperation(String operation) {
			this.operation = operation;
		}


}
