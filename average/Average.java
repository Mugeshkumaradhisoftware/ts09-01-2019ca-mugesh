package average;

public class Average {
	float number1;
	float number2;
	float number3;
	float number4;
	float number5;


	public Average(float number1,float number2,float number3,float number4,float number5) {
		this.number1=number1;
		this.number2=number2;
		this.number3=number3;
		this.number4=number4;
		this.number5=number5;
	
	}
	public float average(){
		float ave=(number1+number2+number3+number4+number5)/5;
		return ave;
	}

	public static void main(String[] args) {
		Average a1=new Average(1,2,3,4,5);
		System.out.println(a1.average());		
	}

}
